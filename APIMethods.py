
import requests
import json
import base64

#все методы используемые в API
url_main = 'https://api.winemback.com'
url_identity = url_main + '/identity'
url_list =  url_main + '/contacts/list'
url_contact =  url_main + '/contacts'
url_scheme = url_main +  '/scheme'
url_scheme_type = url_main + '/scheme-types'


class ApiCommunication ():
    def __init__(self, 
                 token
                 ):
                 
        self.token = token                #токен для работы с API
     
    #первый заголовок для получения токена 
    def __GetHeaderFirst(self, realm, email, password):
        str_usual = realm + ":" + email + ":" + password
        str64 = base64.b64encode(str_usual.encode())
        return { 'authorization': 'Basic %s' % str64.decode()}

    #получаем токен первый раз
    def GetToken(self, realm, email, password):
        print ( self.__GetHeaderFirst(realm, email, password))
        resp = requests.get(url_identity, headers = self.__GetHeaderFirst(realm, email, password)).headers
        return resp['X-Token']
    
    #берем header для работы с API         
    def __GetHeader(self, token):
        return { 'authorization': 'Token token="%s"' % token, 'Content-Type': 'application/json'}
        
    
    #добавить новый контакт 
    def addContact(self, data):
        resp = requests.put(url_contact,  data = json.dumps(data), headers = self.__GetHeader(self.token))
        return resp
        
    #добавить новый контакт
    def addContactList(self, data):
        resp = requests.put(url_list,  data = json.dumps(data), headers = self.__GetHeader(self.token))
        return resp    
 
    #получить текущие аттрибуты схемы
    def getScheme(self, with_type = 0):
        res = {}
        resp = requests.get(url_scheme, headers = self.__GetHeader(self.token)).json()
        if with_type == 0:
            res = [r['name']  for r in resp['scheme']]
        elif with_type == 1:
            for r in resp['scheme']:
                res[r['name']] = r['type']  
        elif with_type == 2:
            for r in resp['scheme']:
                res = [{'name':r['name'], 'type':r['type']}  for r in resp['scheme']]
                
        return res

    #удалить элемент схемы
    def deleteElemFromScheme(self, elem):
        url_delete = url_scheme + '/' + elem
        resp = requests.delete(url_delete,  headers = self.__GetHeader(self.token)).text

    #получить элементы схемы
    def getschemetypes(self):
        res = {}
        resp = requests.get(url_scheme_type, headers = self.__GetHeader(self.token)).json()
        return resp

    #добавить элемент схемы
    def addElem2Scheme(self, elem):
        resp = requests.post(url_scheme,  data = json.dumps(elem), headers = self.__GetHeader(self.token)).json()
        return resp

    
        
        
        