import requests
import threading
import pyodbc
import json
import APIMethods





#нить для параллелизма. Одновременный запуск обновлений  API
#содержит методы для обновления API, а также алгоритм самого обнодления
class ThreadUpdateApi (threading.Thread, APIMethods.ApiCommunication):
    def __init__(self, 
                 token, 
                 contacts, 
                 first_row, 
                 last_row, 
                 num_thread, 
                 logger, 
                 max_threads, 
                 sql, 
                 threads, 
                 clients_sql,
                 cur_working):
        self.token = token                #токен для работы с API
        self.contacts = contacts          #список контактов для update
        self.first_row = first_row        #от контакта до
        self.last_row = last_row          #до контакта 
        self.num_thread = num_thread   
        self.logger = logger      
        self.threadLimiter = threading.BoundedSemaphore(max_threads) #ограничение на максимальный запуск нитей        
        self.sql = sql
        self.threads = threads
        self.clients_sql = clients_sql
        self.cur_working = cur_working
        self.attempts = 3   #кол-во попыток запросы
        threading.Thread.__init__(self)
        APIMethods.ApiCommunication.__init__(self, token)
   
    def run(self):
        self.threadLimiter.acquire()
        try:
            self.updateContact(self.sql, self.threads, self.clients_sql, self.cur_working)
        finally:
            self.threadLimiter.release()

    #берем header для          
    def __GetHeader(self, token):
        return { 'authorization': 'Token token="%s"' % token, 'Content-Type': 'application/json'}
        
    #текст ошибки для событий
    def __GetEventError(self, status_code, text, cur_attemp):
        dic_error = {}
        text = str(text).replace("'",'')[0:500] #для того, чтобы пройти ставку в SQL
        error_text =  "Errors: " + text + str(", code:") + str(status_code) + ", cur attemp " + str(cur_attemp) + "  in thread " + str(threading.current_thread()) 
        error_text += "   , rows " + str(self.first_row) + " – " + str(self.last_row)
        dic_error["NameProc"] = "Retention " + str(self.sql.realmid) 
        dic_error["NameStep"] = text
        dic_error["code_error"] = status_code
        return dic_error   
            
    #текст ошибки для пользователя
    def __GetClientError(self, Identity, DicError):
        dic_error = {}
        dic = {"code":"code_error",  "desc":"code_text", "src": "source"}
        dic_error["siteID"] = Identity
        for key in DicError:
            dic_error[dic[key]] =  str(DicError[key]).replace("'",'')
        return dic_error 


    
    #threads -- статусы нитей для обновления. номер пачки
    def updateContact(self, sql, threads, clients_sql, cur_working):
        errors = [] 
        ids = []
         
        self.cur_working += 1
        self.logger.info("start.update portion in thread № %d, name: %s, rows %d – %d, count working: %d" % (self.num_thread, threading.current_thread(), self.first_row, self.last_row, cur_working) )    
        resp = self.addContactList(self.contacts) #делаем запрос 
        #resp = requests.get("http://yax.com")
        #resp = {"status_code":200}

        #нужно протеститить, что возвращается в случае ошибки
        cur_attemp = 1
        while cur_attemp < self.attempts and resp.status_code != 200: #  попытки. если мы получили ошибку в запросе
            dic_error = self.__GetEventError(resp.status_code, resp.json(), cur_attemp)
            self.logger.info(dic_error["NameStep"])
            sql.logSqlEvent(dic_error)  #прологируем ошибку в SQL
            resp = self.addContactList(self.contacts) #делаем запрос 
            cur_attemp += 1 #переходим к следующей попыткe 

        data = resp.json()
        if resp.status_code != 200: #если не все хорошо
            dic_error = self.__GetEventError(resp.status_code, resp.json(), cur_attemp)
            self.logger.info(dic_error["NameStep"] )
            self.logger.info("finish.update error: %s in thread %s, rows %d – %d" % (dic_error["NameStep"],  threading.current_thread(), self.first_row, self.last_row) )   
            if "Message" in data:  #если нет никакой доп. информации 
                del self.contacts
                cur_working -= 1 
                self.logger.info("finish.update FAIL in thread № %d, name %s, rows %d – %d. ok -  %d,  fail - %d, count working: %d" % (self.num_thread, threading.current_thread(), self.first_row, self.last_row, 0, self.last_row-self.first_row, cur_working ) )    
                return  data
        
        #data = {}
        if (resp.status_code  != 500):
            for client in data:                                                       #каждый контакт из попытки запроса
                if client["success"] != True:                                         #если обнаружилась ошибка при вставке/обновлении
                    dic_error =  self.__GetClientError(client["source"]["Identity"], client["error"])
                    errors.append(dic_error)
                else:
                    ids.append(str(client["source"]["Identity"]))
                    self.clients_sql[str(client["source"]["Identity"])]["status"] = 1
            
        if errors != []:
            for error in errors: #для каждого клиента
                try: 
                    sql.logSqlContact(dic_error)
                    self.clients_sql[str(error["siteID"])]["status"] = -1  
                except:
                    self.logger.info(" no key %s " % str(error["siteID"]))   
        
        if ids  == []: #если нет ни одного проапдейченного id-шника 
            flag_error = 1
            threads[self.num_thread]["status"] = -1 #не было обновленно ни одного контакта
            self.logger.info("finish.update No contacts in thread  № %d, name %s, rows %d – %d" % (self.num_thread, threading.current_thread(), self.first_row, self.last_row) )    
            del self.contacts 
            return    
     
        
        threads[self.num_thread]["status"] = 1 #флаг, что нить для обновления завершена
            
        self.cur_working -= 1    
        self.logger.info("finish.update OK in thread № %d, name %s, rows %d – %d. ok -  %d,  fail - %d, count working: %d" % (self.num_thread, threading.current_thread(), self.first_row, self.last_row, len(ids), len(errors), cur_working ) )    
        del self.contacts
        return data

        