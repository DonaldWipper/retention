import SQLOperations  #кастомный модуль для работы с SQL
import threading 
import pyodbc
import datetime
#class for updated data

#нить для обновления даты в таблице SQL         
class ThreadUpdateSQL (threading.Thread, SQLOperations.operationsSQL):
    def __init__(self, realmid,           #id project 
                       mm_query_params,   #query params for uploading data
                       sql_connection,    #sql params 
                       emails_log,        #address for senting to email  
                       logger,            #global logger
                       offset,            #порция обновления в API
                       max_threads,       #максимальное одновременное кол-во нитей 
                       threads,
                       clients_sql
                            
                       ):                 #параметры для подключения и кол-во контактов в одной нити

        SQLOperations.operationsSQL.__init__(self,  realmid, mm_query_params, sql_connection,  emails_log, logger)
        threading.Thread.__init__(self)
        self.qty_uploading_records = 0 # qty_uploading_records should be checked in the end 
        self.status = False            # статус нити  
        self.max_threads  = max_threads
        self.threads = threads
        self.clients_sql = clients_sql
        self.threadLimiter = threading.BoundedSemaphore(max_threads) #ограничение на максимальный запуск нитей
        self.offset = offset
      
    def run(self):
        self.threadLimiter.acquire()
        try:
            self.functionForUpdating(self.threads, self.clients_sql)
        finally:
            self.threadLimiter.release()
    
    #взять список клиентов по смещниею в списке клиентов 
    def __getListFromDicByOffset(self, client_list, i, Offset):
        length = len(client_list)
        cur_row = i * Offset  
        if  cur_row >= length:
            return []   
        last_row = min(cur_row + Offset - 1, length - 1)
        print("cur row %d, last row %d"  % (cur_row, last_row))  
        return  [client for client in  client_list if client_list[client]["number"] >= cur_row and client_list[client]["number"] <= last_row]

        
    #ставим новую дату для записей, которые обновились в емарсисе
    def updateLogSql(self, contacts):
        if contacts == []:
            return 
        client_list = "("
        for key in contacts:
            #print(str(self.clients_sql[key]) + ", " + str(x) + "," + str(thread))
            try:
                
                if self.clients_sql[key]["status"] == 1:
                    self.qty_uploading_records += 1
                    client_list += "'" + key + "',"
                    self.clients_sql[key]["status"] = 2
            except:
                self.logger.info("no key %s " % str(key)) 
        client_list += ")"
        client_list = client_list.replace(',)', ')')
        query = "UPDATE t"
        query += " SET t.DateFirstUpdRetention = CASE WHEN t.DateUpdRetention IS NULL THEN "  + " '" +datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S") + "\' " + " ELSE t.DateFirstUpdRetention  END " #первый раз обновляем, если еще никогда не было даты обновления
        query += " FROM " + self.table_base + " t "
        if self.realmid in [5, 14, 16]: 
            query += " WHERE ClientID IN " + str(client_list)
        else:
            query += " WHERE SiteID IN " + str(client_list)
        
        #self.logger.info(query)
        self.cursor.execute(query) 
        self.cnxn.commit()
        
        query = "UPDATE t"
        query += " SET t.DateUpdRetention = " + "\'" +datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S") + "' "  
        query += " FROM " +  self.table_base + " t "
        if self.realmid in [5, 14, 16]: 
            query += " WHERE ClientID IN " + str(client_list)
        else:
            query += " WHERE SiteID IN " + str(client_list)
       
        self.cursor.execute(query) 
        self.cnxn.commit()
               
               
        del contacts
        del client_list
    
    def functionForUpdating(self, threads, clients_sql):
        self.cnxn = pyodbc.connect(self.connect_str)
        self.cursor = self.cnxn.cursor()    
    
        while not self.status:
            self.status = True
            for key in self.threads:
                #print("upd sniff " + str(key))
                if threads[key]["status"] not in [2, -1]:   #если есть непроставленный готовый статус
                    self.status = False      #пока работаем
                    
                if  threads[key]["thread"].is_alive() == False and threads[key]["thread"] != -1: #если нить умерла
                    if threads[key]["status"] == 1:             #данные были отправлены данные в Retention
                        contacts = self.__getListFromDicByOffset(clients_sql, key, self.offset)
                        #print("contacts " + str(contacts) + " " +  str(key) + ", " + str(self.offset) + ", " + str(clients_sql) )
                        self.updateLogSql(contacts)
                        self.logger.info("finish.update DateUpdRetention in SQL. OK in  %s " % threading.current_thread())  
                        threads[key]["status"] = 2
                        del contacts
                    else:
                        if threads[key]["status"] != 2:
                            threads[key]["status"] = -1      
        if self.status == True:
            return         


            



    
            
            