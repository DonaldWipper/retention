import pyodbc
import logging
import json
import datetime

#class for init SQL connection


#здесь все операции и запросы в SQL    
class operationsSQL():
    def __init__(self, realmid,           #id project 
                       mm_query_params,   #query params for uploading data
                       sql_connection,    #sql params 
                       emails_log,        #address for senting to email  
                       logger             #global logger
                       ):
    
        self.mm_proc = sql_connection["exec_proc"]
        self.mm_query_params = mm_query_params
        self.logger = logger;
        self.realmid = realmid
        self.emails_log = emails_log
        self.log_tables_client = sql_connection["log_table_client"] #logging by client
        self.log_event = sql_connection["log_table"]                #logging events
        self.table_base = sql_connection["table_base"]              #logging uploading in mm table
        self.connect_str =  'DRIVER={' + sql_connection["driver"] 
        self.connect_str += '};SERVER=' + sql_connection["server"] 
        self.connect_str += ';DATABASE=' + sql_connection["database"]
        self.connect_str += ';UID='+ sql_connection["username"] 
        self.connect_str += ';PWD='+ sql_connection["password"]
        self.table_projects = sql_connection["projects_realm"] 
        self.cnxn = pyodbc.connect(self.connect_str)
        self.cursor = self.cnxn.cursor()  
        if self.realmid not in [14, 5, 16]:
            self.table_base = self.table_base +  self.__getShortNameProjectBeRealm() + "_V3"
            if self.realmid == 17:
                self.table_base = self.table_base.replace('nRB', 'RB')    
        else:
            self.table_base = self.table_base
        self.short_name = self.__getShortNameProjectBeRealm().lower()
        
    #делаем реиницилизацию, если что-то падает
    def __reinitSQL(self):
        del self.cursor
        del self.cnxn
        self.cnxn = pyodbc.connect(self.connect_str)
        self.cursor = self.cnxn.cursor() 
    
    def closeConnection(self):
        self.cnxn.close()
    
    
    #кол-во строк в запросе    
    def getCountRows(self):
        #Sample select query
        rows = 0
        str_query = "EXEC " + self.mm_proc
        str_query += (",").join(["@" + str(par) + "=" + str(self.mm_query_params[par]) for par in self.mm_query_params]) + ", @TYPE_SCRIPT = 1"
        self.logger.info(str_query)
        self.cursor.execute(str_query) 
        row = self.cursor.fetchone() 
        while row:
           return int(row[0])
     
    #получить имя проекта по realm 
    def __getShortNameProjectBeRealm(self):
        rows = 0
        res = "FW"
        str_query = "SELECT ShortName FROM " + self.table_projects  + " WHERE  RealmID = " + str(self.realmid)
        self.cursor.execute(str_query) 
        row = self.cursor.fetchone() 
        while row:
            res = str(row[0])
            if self.realmid == 17:
                res = "n" + res
            return res 

    def getShortName(self):
        return self.short_name
            
     
    #получаем названия полей с их типом
    def getTitlesSql(self):
        str_query = "EXEC " + self.mm_proc
        query_params = self.mm_query_params
        query_params["Offset"] = 1   #this one gives just 1 row of query
        query_params["NumIter"] = 1
        str_query += (",").join(["@" + str(par) + "=" + str(query_params[par]) for par in query_params]) + ",@TYPE_SCRIPT = 0"
        self.logger.info(str_query)
        self.cursor.execute(str_query) 
        titles_sql = [ {"name":i[0].lower(), "type":i[1].__name__} for i in self.cursor.description]
        return titles_sql
      
    #вставка данных в таблицу  
    def insertIntoTable(self, table, params):
        query = " INSERT INTO " + table + " ("
        values = ""
        query += (",").join([str(par) for par in params])
        query += ")"
        query += "values " +  "(" + (",").join([ "'" + str(par) + "'" for par in params.values()]) + ")"
        
        try:
            self.logger.info(query)
            self.cursor.execute(query)
        except:
            self.logger.info("fuckupquery " + query)
        self.cnxn.commit()
  
    #{"siteID":1, "code_error":-1, "code_text":"test", "source": "is_coreg"}          
    # логирование каждого незагруженного контакт
    def logSqlContact(self, params):
        params["date_upload"] = datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S") 
        self.logger.info(str(params))
        self.insertIntoTable(self.log_tables_client, params)
        
    #ID NumStep NameProc NameStep 
    #логирование событий. В таблицу SQL 
    #{"NumStep":1, "NameProc": "Retention", "NameStep":"test", "code_error": 400} 
    def logSqlEvent(self, params):
        params["timelog"] = datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S")
        if "code_error" in params and int(params["code_error"]) != 500:
            self.insertIntoTable(self.log_event, params)

    #процедура для получения порции данных    
    def exec_proc(self, Offset, NumIter):
        str_query = "EXEC " + self.mm_proc
        str_query += (",").join(["@" + str(par) + "=" + str(self.mm_query_params[par]) for par in self.mm_query_params if str(par) not in ('Offset', 'NumIter')]) + ", @TYPE_SCRIPT = 0"
        str_query += ", @Offset = "  + str(Offset) 
        str_query += ", @NumIter = " + str(NumIter) 
        self.logger.info(str_query)
        qty_try = 2 #2 попытки выполнить процедуру
        num_try = 0
        while num_try < qty_try:
            try:
                self.cursor.execute(str_query)
                break;
            except:
                self.logger.info("Error of execution sql, № try %d " % num_try)
                self.closeConnection()
                self.__reinitSQL()
            num_try += 1    
        
    #процедура для отправки нотификаций на почту
    def sent_email(self, text):
        str_query =  "EXEC msdb.dbo.sp_send_dbmail "
        str_query += " @recipients = " + "'" + self.emails_log + "'"
        str_query += ",@body = " + "'" + text + "'"
        str_query += ",@subject = " + "'" + "information about uploading into retention" + "'" 
        self.logger.info(str_query)
        self.cursor.execute(str_query)
        self.cnxn.commit()
        

    

    