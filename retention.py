import pyodbc
import logging
import json
import datetime
import SQLOperations  #кастомный модуль для работы с SQL
import SQLUpdatingThread
import UpdateApiThread
import APIMethods
import threading
import requests
import gc
import sys
import math
from decimal import Decimal


#словарь соответсвия проекта и сервера данных
realm2server = {20:  "sql_statdc", 
                22: "sql_statdc", 
                3: "sql_statdc", 
                14: "sql_stat", 
                16: "sql_stat3", 
                15: "sql_statdc",
                17: "sql_statdc",
                10: "sql_statdc",
                8:  "sql_statdc",
                11: "sql_statdc"}
cur_working = 0
logger = None 

threadLimiter = None #ограничитель на кол-во нитей
   
#считываем настройки
def read_params(fn): 
    d ={} 
    try:
        with open(fn, 'r',encoding="utf-8") as file: 
            d = json.load(file) 
    except FileNotFoundError:
         print ("Error. Can't find file " + fn)
         d = {}
    return d 


       
clients_sql = {}      #cписок клиентов для update
threads = {}          #статус нитей {num_thread: {"id_thread": id_thread, "status": status}
threads_active = []   #запущенные нити
max_threads = 0
token = None
 
#массив, в который собираем контакты для обновления
contact_list_global = []    

#кастомные поля для загрузки адресов приложений
ios_address = "ApplePushTokens"
android_address = "GooglePushTokens"

#поля, которые мы почему-то не видем в интерфейсе
hidenFields = [ios_address, android_address, "Emails", "Timezone"]

#поля, которые есть в Retention
retentionFields = []
retention_fields_types = {}


#поля названия которых нужно будет поменять 
accordingFields = {"id":"Identity", "app_address_ios":ios_address, "app_address_android":android_address, "email":"Emails", "timezone":"Timezone", "first_name":"FirstName"}

result_upload = {"total":0, "success":0, "log_sql":0}

def setList(address):
    ads = address.split(',')
    if len(ads) == 1:
        del ads
        return address
    else:
        del ads
        return address[0]
  

#функция для update загружаем
def updateContactValue(value,  key):
    global retention_fields_types
    if ((key in retention_fields_types) and value.lower() in ['false', 'true']): #бывает, что у типа например имя True
        if (retention_fields_types[key] != 'boolean'):
            return value  
               
    if ((key in retention_fields_types) and (retention_fields_types[key] == 'numeric')):
        return int(math.floor(float(value)))  
    #if type(str) == type(list()):
    #    return str
    if ((value.lower() == '0') and (retention_fields_types[key] == 'boolean')):
        return False
    if ((value.lower() == '1') and (retention_fields_types[key] == 'boolean')):
        return True
    if value.lower() == 'false':
        return False
    elif value.lower() == 'true':
        return True
    elif value.lower() == 'none':
        return None
    elif value.lower() == 'no information':
        return None
    elif value.lower() == '':
        return None
    else:
        return value
        

def updateValuesInContact(contact):
    for key in contact:
        if key in ["ApplePushTokens", "GooglePushTokens"]:
            continue;
        contact[key] = updateContactValue(str(contact[key]), key)

        
        
  
def getClearContact(row):
    global sql, retentionFields
    contact = {}
    for idx in range(len(row)):
        title = sql.cursor.description[idx][0].lower()   #получим название заголовков
        contact[title] =  str(row[idx])               
        #кастомные поля в системе retention
        if title in accordingFields:
            contact[accordingFields[title]] = contact[title]        
        del title
    #удалим другие названия
    for key in accordingFields:
        if key in contact:
            del contact[key]
    
    #для неподтвержденных не передаем Email
    if contact["confirmation"] == False: 
        del contact["Emails"]
            
            
    #специфика для передачи адресов. где может быть несколько значений    
    for key in [ios_address, android_address]:
        contact[key] = setList(contact[key])     
   
    
    list_field = [c for c in contact]
    #удалим все пустые поля
    for c in list_field :
        if (str(contact[c]) == 'None' or contact[c] == ['None']):
            if (c in [android_address, ios_address]):
                contact[c] = None
            else:   
                del contact[c] 
                continue;
        #если находится не в списке выгружаемых полей        
        if c not in retentionFields:
            del contact[c] 
    updateValuesInContact(contact) 
    if contact["confirmation"] == False: #для неподтвержденных не передаем Email
        if "Emails" in contact:
            del contact["Emails"]
    del list_field
    del row
    
    #if (contact["Identity"] in ['56309367831']):
    #    print(contact)
    #    return;
    return contact    
        

        
def updateContacts(Offset, NumIter):
    #нити, статусы обновления клиентов, пул для обновления 
    global threads, clients_sql, contact_list_global, logger, max_threads, token, sql, result_upload, cur_working 
    
    #запуск процедуры сбора данных 
    logger.info("start.exec sql offset = %d , NumIter = %d" % (Offset, NumIter) )
    sql.exec_proc(Offset, NumIter)
    logger.info("finish.exec sql offset = %d , NumIter = %d" % (Offset, NumIter) )
    
    
    qty_portions = 1     #кол-во последоватьльных порций для обновления
    update_portion = 1   #кол-во строк в одном обновлении
 
    logger.info("start.saving sql table in memory  = %d , NumIter = %d" % (Offset, NumIter) )
    rows = sql.cursor.fetchall() #считаем все строки 
    qty_rows = len(rows)
    logger.info("finish.saving sql table in memory  = %d , NumIter = %d" % (Offset, NumIter) )
    
    offset_local = min(qty_rows, Offset) #если строк меньше, чем запущененных в загрузке SQL
 
    
    if  offset_local  < settings["offset_api"] * settings["count_threads"]: #ecли данных меньше, чем одна порция
        update_portion  = offset_local
        qty_portions = 1
    else:       
        update_portion = settings["offset_api"] * settings["count_threads"]                
        qty_portions = offset_local // update_portion + 1

    logger.info("count portions is %d" % qty_portions )
    
    
    logger.info("start.create clear list for sending to Retention Offset = %d , NumIter = %d" % (Offset, NumIter) )
    result_upload["total"] += len(rows)

    contact_list_global = []
    cur_client = 0    
    i = 0
    for row in rows:
        contact = getClearContact(row)     #получим приведенные данные
        contact_list_global.append(contact)    #заполним массив для обновления   
        clients_sql[contact["Identity"]] = {"status":0, "number":cur_client} #флаг 0 означает, что пока нет ошибки
        cur_client += 1
        del contact
        #gc.collect()
    del rows
    logger.info("finish.create cleat list for sending to Retention Offset = %d , NumIter = %d" % (Offset, NumIter) )  
    gc.collect()
    
    #нить для я обновления SQL
    #старт до начала обновления API 
      
    sqlUpdate = SQLUpdatingThread.ThreadUpdateSQL(realmid, query_params, settings[realm2server[realmid]], settings["emails_sent"], logger, settings["offset_api"], settings["count_threads_limit"], threads, clients_sql)
    sqlUpdate.start()
    
    cur_row = 0 
    num_thread = -1
    for cur_portion in range(qty_portions):                       #порции для параллелизма
        for cur_thread in range(settings["count_threads"]):       #цикл по нитям внутри порции 
            last_row = min(cur_row + settings["offset_api"], len(contact_list_global)) #последняя строка обновления
            contact_list = contact_list_global[cur_row:last_row]  #массив для обновления нити
            if contact_list != []:
                num_thread += 1
                threadUpdate = UpdateApiThread.ThreadUpdateApi(token, contact_list, cur_row, last_row-1, num_thread, logger, max_threads, sql, threads, clients_sql, cur_working)
                threads[num_thread] = {"id_thread": -1, "status": 0, "thread":threadUpdate}
                threadUpdate.start()  
                threads[num_thread]["id_thread"] = threadUpdate.ident
                
            del contact_list
            cur_row += settings["offset_api"]
        if last_row == len(contact_list_global) - 1: 
            break
    for cur_thread in threads:
        threads[cur_thread]["thread"].join()
    sqlUpdate.join()  
    
    result_upload["success"] += sum([1 for c in clients_sql if clients_sql[c]["status"] == 1 or clients_sql[c]["status"] ==  2])
    result_upload["log_sql"] += sum([1 for c in clients_sql if clients_sql[c]["status"] == 2])
    del contact_list_global
    del clients_sql
    del threads
    
     
    clients_sql = {}
    contact_list_global = []    
    threads = {}
    gc.collect() 
    
    assert not gc.garbage, "Object leak: %s" % str(gc.garbage) 
    logger.info("finish. global update iters %d -- %d, count active threads: %d" % (NumIter * settings["offset_sql"], (NumIter + 1) * settings["offset_sql"], threading.active_count()))     
    #threadLog.finish()
    
    
    

def setLoggerFormat():
    global logger
    logger = logging.getLogger(__name__)
    logger.setLevel(logging.INFO)
    
    console = logging.StreamHandler()
    console.setLevel(logging.INFO)

    # create a file handler
    fo = open("logs.txt", "w")
    fo.close()
    handler = logging.FileHandler('logs.txt')
    handler.setLevel(logging.INFO)

    # create a logging format
    formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
    handler.setFormatter(formatter)
    
    console.setFormatter(formatter)
    # add the handler to the root logger
    logging.getLogger('').addHandler(console)
    logger.addHandler(handler)
  
 
def UPLOAD_DATA_API(IsConfirmed = True, HasAppAdress = None):
    global qty_uploading_records, sql, logger, clients_sql, result_upload
    result_upload = {"total":0, "success":0, "log_sql":0}
    qty_uploading_records = 0
    logger.info("start.get count rows of query")
    rows = sql.getCountRows() 
    logger.info("finish.get count rows of query. Count rows is %d" % rows)
    qty_iters = rows // settings["offset_sql"] + 1 #кол-во итераций по порциям запроса в SQL
    sql_event = {"NumStep":1, "NameProc": "Retention " + sql.getShortName(), "NameStep":"Final info", "code_error":-1}
    
    
    for i in range(qty_iters):
        logger.info("start. global update iters %d -- %d" % (i * settings["offset_sql"], (i + 1) * settings["offset_sql"])  )
        updateContacts(settings["offset_sql"], i)
        gc.collect()
    sql_event["NameStep"] =  "Finish uploading isConfirmed = %s, HasAppAdress = %s. Total:%d, success:%d, fail:%d, logged in sql: %d" % ( str(IsConfirmed), str(HasAppAdress), result_upload["total"], result_upload["success"], result_upload["total"] - result_upload["success"], result_upload["log_sql"])
    sql.logSqlEvent(sql_event)
    
   
    
    
    
if __name__ == '__main__':
    if len(sys.argv) < 2:
        realmid = 20 
    else:
        try:
            realmid = int (sys.argv[1]) 
        except ValueError:
            print("Ошибка. Введенные аргументы не являются вещественными числами")
            exit()
            
            
    setLoggerFormat()                                #инициализируем логгер
    #gc.set_debug(gc.DEBUG_SAVEALL)     
    gc.enable()
    settings = read_params("settings.json")           #считываем параметры SQL и нитей
    query_params = read_params("query_params.json")   #параметры запроса для вгрузки в емарсис
    query_params["realmid"] = realmid
    max_threads = settings["count_threads_limit"]
    threadLimiter = threading.BoundedSemaphore(settings["count_threads_limit"]) #ограничение на максимальный запуск нитей
    
    #3 этапа. запуск нити для логинга, запуск процесса обновления API, ожидания завершения обновления API
        
    logger.info("start.init SQL")
    sql = SQLOperations.operationsSQL(realmid, query_params, settings[realm2server[realmid]], settings["emails_sent"], logger) #кастомный класс для работы с SQL
    token = settings["retention_" + sql.getShortName()]["token"]
    logger.info("finish.init SQL")
    
    
    api_meths = APIMethods.ApiCommunication(token)
    retentionFields = api_meths.getScheme() + hidenFields
    retention_fields_types = api_meths.getScheme(1) 
    print(retention_fields_types)
    
    
    UPLOAD_DATA_API(IsConfirmed = True, HasAppAdress = None)
    
    sql.closeConnection()
    del sql
    sql = SQLOperations.operationsSQL(realmid, query_params, settings[realm2server[realmid]], settings["emails_sent"], logger) #кастомный класс для работы с SQL
    query_params = read_params("query_params_with_devices.json")   #параметры запроса для выгрузки в retention для устройств с адресами приложений
    query_params["realmid"] = realmid  
    UPLOAD_DATA_API(IsConfirmed = False, HasAppAdress = True)
    
    #unconfirmed
    sql.closeConnection()
    del sql
    sql = SQLOperations.operationsSQL(realmid, query_params, settings[realm2server[realmid]], settings["emails_sent"], logger) #кастомный класс для работы с SQL
    query_params = read_params("query_params_unsubscribe.json")   #параметры запроса для выгрузки в retention
    UPLOAD_DATA_API(IsConfirmed = False)
    query_params["realmid"] = realmid   
   
    
    #print(sql.getCountRows())
    #print(sql.exec_proc(0,1))
    #print(sql.getTitlesSql())
    #print(sql.logSqlContact({"siteID":1, "code_error":-1, "code_text":"test"}))
    #print(sql.logSqlEvent({"NumStep":1, "NameProc": "Retention", "NameStep":"test"}))
    